
// Mad Lib
// Brendan Skelton

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void PrintOut(string answers[12], ostream &out = cout)
{

	out << "\nOne day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ".\n";
	out << "We really wanted to see " + answers[3] + " play.\n";
	out << "So we " + answers[4] + " in the " + answers[5] + " and headed down to " + answers[6] + " and bought some " + answers[7] + ".\n";
	out << "We watched the game and it was " + answers[8] + ".\n";
	out << "We ate some " + answers[9] + " and drank some " + answers[10] + ".\n";
	out << "We had a " + answers[11] + " time, and can't wait to go again.";
}

int main()
{

	string question[12] = { "Enter an adjective (describing word): ",
						    "Enter a sport: ",
						    "Enter a city name: ",
						    "Enter a person: ",
							"Enter an action verb (past tense): ",
							"Enter a vehicle: ",
							"Enter a place: ",
							 "Enter a noun (thing, plural): ",
							"Enter an adjective (describing word): ",
							"Enter a food (plural): ",
							"Enter a liquid: ",
							"Enter an adjective (describing word): "};

	string answers[12];
	for (int i = 0; i < 12; i++)
	{
		cout << question[i];
		getline(cin, answers[i]);
	}

	PrintOut(answers);

	char input = 'n';
	cout << "\nSave to file? (y/n)";
	cin >> input;

	if (input == 'y')
	{
		ofstream ofs("C:\\Temp\\mad.txt");
		if (ofs) PrintOut(answers, ofs);
		ofs.close();
	}
	//ofstream ofs;
	//ofs.open("C:\\Temp\\text.txt");
	//if (ofs)
	//{
	//	ofs << PrintOut(answers);
	//}

	_getch();
	return 0;
}
